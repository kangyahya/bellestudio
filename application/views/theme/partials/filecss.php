<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Belle Studio</title>

  <!-- Bootstrap core CSS -->
  <link href="<?=base_url()?>assets/web/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/fonts/font-awesome.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/web/css/shop-homepage.css" rel="stylesheet">
</head>