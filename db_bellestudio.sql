-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 02 Jul 2020 pada 09.07
-- Versi server: 10.3.22-MariaDB-1
-- Versi PHP: 7.3.15-3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bellestudio`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_detail_paket`
--

CREATE TABLE `tbl_detail_paket` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `uid_paket` varchar(100) NOT NULL,
  `included` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_detail_paket`
--

INSERT INTO `tbl_detail_paket` (`id`, `uid`, `uid_paket`, `included`) VALUES
(1, 'a1b8cc16-b709-11ea-ad03-c0535ee047c0', '101d7b6a-b706-11ea-ad03-c0535ee047c0', '2 jam pemotretan'),
(2, 'a1b8dd7f-b709-11ea-ad03-c0535ee047c0', '101d7b6a-b706-11ea-ad03-c0535ee047c0', 'Cetak 20RP + Bingkai (2) Pcs'),
(6, 'ef501cef-b709-11ea-ad03-c0535ee047c0', '101d7b6a-b706-11ea-ad03-c0535ee047c0', 'Cetak 4R + Bingkai (5) Pcs'),
(7, 'ef502c35-b709-11ea-ad03-c0535ee047c0', '101d7b6a-b706-11ea-ad03-c0535ee047c0', 'Make’up dan Baju 1 Set'),
(8, 'ef503584-b709-11ea-ad03-c0535ee047c0', '101d7b6a-b706-11ea-ad03-c0535ee047c0', 'Soft Copy All File (DVD)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_faq`
--

CREATE TABLE `tbl_faq` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `ask` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_jadwal`
--

CREATE TABLE `tbl_jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `uid_paket` varchar(100) NOT NULL,
  `uid_member` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_member`
--

CREATE TABLE `tbl_member` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `type_member` enum('super','basic') NOT NULL DEFAULT 'basic',
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_member`
--

INSERT INTO `tbl_member` (`id`, `uid`, `username`, `password`, `type_member`, `nama`, `email`, `nohp`, `alamat`) VALUES
(1, '614e1999-b9b1-11ea-abc0-c018856b5741', 'yahya', '59202463fd4c312b063293b88f6063b2', 'basic', 'Moh. Yahya', 'moch.yahya95@gmail.com', '+6289656729025', 'Setu'),
(2, '767c2a0b-b9cb-11ea-abc0-c018856b5741', 'kangyahya', 'd41d8cd98f00b204e9800998ecf8427e', 'basic', 'Kang Yahya', 'moch.yahya95@gmail.com', '+6289656729025', 'Desa setupatok'),
(5, 'c071d939-bacb-11ea-8603-c018856b5741', 'lana', 'd62af190feaaef135566b28fe9b1bb80', 'basic', 'lana', 'inikangyahya@gmail.com', '089656729025', 'lana');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_note`
--

CREATE TABLE `tbl_note` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `uid_paket` varchar(100) NOT NULL,
  `note` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_paket`
--

CREATE TABLE `tbl_paket` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `paket` varchar(75) NOT NULL,
  `image` varchar(100) NOT NULL,
  `harga` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_paket`
--

INSERT INTO `tbl_paket` (`id`, `uid`, `paket`, `image`, `harga`) VALUES
(1, '101d7b6a-b706-11ea-ad03-c0535ee047c0', 'Pre-Wedding Indoor', '101d7b6a-b706-11ea-ad03-c0535ee047c0.jpg', 1000000),
(2, '0c1791d8-b7cd-11ea-a8ef-c018856b5741', 'Pre-Wedding Outdoor', '0c1791d8-b7cd-11ea-a8ef-c018856b5741.jpg', 1500000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_reservation`
--

CREATE TABLE `tbl_reservation` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `uid_paket` varchar(100) NOT NULL,
  `uid_member` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `status` enum('lunas','belum') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_reservation`
--

INSERT INTO `tbl_reservation` (`id`, `uid`, `uid_paket`, `uid_member`, `tanggal`, `jam_mulai`, `jam_selesai`, `status`) VALUES
(1, 'c32b65fc-bafa-11ea-8603-c018856b5741', '0c1791d8-b7cd-11ea-a8ef-c018856b5741', '614e1999-b9b1-11ea-abc0-c018856b5741', '2020-07-02', '01:54:00', '01:54:00', 'belum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tambahan`
--

CREATE TABLE `tbl_tambahan` (
  `id` int(11) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `uid_paket` varchar(100) NOT NULL,
  `tambahan` varchar(50) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_tambahan`
--

INSERT INTO `tbl_tambahan` (`id`, `uid`, `uid_paket`, `tambahan`, `qty`, `harga`) VALUES
(1, 'fc636bf7-baf4-11ea-8603-c018856b5741', '0c1791d8-b7cd-11ea-a8ef-c018856b5741', '1 Orang', 1, 30000),
(2, 'fd42653b-baf6-11ea-8603-c018856b5741', '0c1791d8-b7cd-11ea-a8ef-c018856b5741', 'Cetak 5R + Bingkai', 1, 30000);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_detail_paket`
--
ALTER TABLE `tbl_detail_paket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`),
  ADD KEY `uid_paket` (`uid_paket`);

--
-- Indeks untuk tabel `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indeks untuk tabel `tbl_jadwal`
--
ALTER TABLE `tbl_jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indeks untuk tabel `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_note`
--
ALTER TABLE `tbl_note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid_detail` (`uid_paket`);

--
-- Indeks untuk tabel `tbl_paket`
--
ALTER TABLE `tbl_paket`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indeks untuk tabel `tbl_reservation`
--
ALTER TABLE `tbl_reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_tambahan`
--
ALTER TABLE `tbl_tambahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid_paket` (`uid_paket`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_detail_paket`
--
ALTER TABLE `tbl_detail_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_jadwal`
--
ALTER TABLE `tbl_jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_note`
--
ALTER TABLE `tbl_note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_paket`
--
ALTER TABLE `tbl_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_reservation`
--
ALTER TABLE `tbl_reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_tambahan`
--
ALTER TABLE `tbl_tambahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_detail_paket`
--
ALTER TABLE `tbl_detail_paket`
  ADD CONSTRAINT `tbl_detail_paket_ibfk_1` FOREIGN KEY (`uid_paket`) REFERENCES `tbl_paket` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_note`
--
ALTER TABLE `tbl_note`
  ADD CONSTRAINT `tbl_note_ibfk_1` FOREIGN KEY (`uid_paket`) REFERENCES `tbl_detail_paket` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_tambahan`
--
ALTER TABLE `tbl_tambahan`
  ADD CONSTRAINT `tbl_tambahan_ibfk_1` FOREIGN KEY (`uid_paket`) REFERENCES `tbl_paket` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
